#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<math.h>
#include<queue>
using namespace std;
 
struct Point {
	float x,y;
	Point(){}
	Point(float x, float y) {
		this->x=x;
		this->y=y;
	}
};
 
struct Node {
	Node* prev;
	Node* succ;
	int id, rank;
	Node(){}
	Node(Node* prev, int id) {
		this->rank=prev->rank+1;
		this->id=id;
		this->prev=prev;
		prev->succ=this;
	}
	Node(int id) {
		this->rank=0;
		this->id=id;
		this->prev=NULL;
		this->succ=NULL;
	}
};
void createDistanceMatrix();
void getNearestMatrix();
Node* constructGreedyTour(const int);
void LKH(Node*);
bool between(const Node*, const Node*, const Node*);
bool do2Opt(Node*, Node*);
bool do3Opt(Node*, Node*, Node*, Node*, int, int);
void swap2(Node*, Node*, Node*);
void swap3(Node*, Node*, Node*, Node*, Node*, Node*, int);
 
inline double round(double x) { return (x-floor(x))>0.5 ? ceil(x) : floor(x); }
 
int dMat[1000][1000];
int nMat[1000][100];
int nElem;
int loopParam[2];
Node* nodeById[1000];
int N;
int maxk;
int main(int argc, char* argv[])
{
	scanf("%d", &N);
	createDistanceMatrix();
	if(N<150) {
		nElem=min(N-1,20);
		loopParam[0]=min(N-1,20);
		loopParam[1]=min(N-1,15);
	}
	else if(N<500) {
		nElem=min(N-1,18);
		loopParam[0]=min(N-1,18);
		loopParam[1]=min(N-1,16);
	}
	else if(N<750) {
		nElem=min(N-1,14);
		loopParam[0]=min(N-1,14);
		loopParam[1]=min(N-1,12);
	}
	else {
		nElem=min(N-1,12);
		loopParam[0]=min(N-1,12);
		loopParam[1]=min(N-1,10);
	}
	getNearestMatrix();
	maxk=min(900000000/(N*N),1800);
	Node* start = constructGreedyTour(0);
	LKH(start);
	Node* n = start;
	do {
		printf("%d\n", n->id);
		n=n->succ;
	} while(n!=start);
	return 0;
}
 
void createDistanceMatrix() {
	Point points[1000];
	for(int i=0;i<N;i++) {
		float x,y,a;
		//scanf("%f %f %f", &a, &x, &y);
		scanf("%f %f", &x, &y);
		points[i]=Point(x,y);
	}
	for(int i=0;i<N;i++) {
		for(int j=i;j<N;j++) {
			Point p1=points[i];
			Point p2=points[j];
			int dist=(int)round(sqrt((p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y)));
			dMat[i][j]=dist;
			dMat[j][i]=dist;
		}
	}
}
 
void getNearestMatrix() {
	for(int i=0;i<N;i++) {
		bool used[1000]={false};
		used[i]=true;
		for(int k=0;k<nElem;k++) {
			int best=-1;
			for(int j=0;j<N;j++)
				if(!used[j] && (best==-1 || dMat[i][j]<dMat[i][best]))
					best=j;
			nMat[i][k]=best;
			used[best]=true;
		}
	}
}
 
Node* constructGreedyTour(int s) {
	bool used[1000]={false};
	if(s<0)
		s=rand()%N;
	Node* start = new Node(s);
	used[s]=true;
	Node* n=start;
	nodeById[n->id]=n;
	for(int i=1;i<N;i++) {
		int best=-1;
		for(int j=0;j<N;j++) {
			if(!used[j] && (best==-1 || dMat[n->id][j]<dMat[n->id][best]))
				best=j;
		}
		nodeById[best]=new Node(n,best);
		used[best]=true;
		n=n->succ;
	}
	n->succ=start;
	start->prev=n;
	return start;
}
 
void LKH(Node* start) {
	int k=0;
	bool improved=false;
	do {
		improved=false;
		Node* n=start;
		do {
			Node* t1=n;
			Node* t2 = t1->succ;
			improved=do2Opt(t1,t2);
			if(improved) {
				k++;
				break;
			}
			n=n->succ;
		} while(n!=start);
	} while(improved && k<maxk);
}

bool between(const Node* a, const Node* b, const Node* c) {
	if(a->rank<=c->rank) {
		return b->rank>=a->rank&&b->rank<=c->rank;
	}
	return b->rank>=a->rank || b->rank<=c->rank;
}
 
bool do2Opt(Node* t1, Node* t2) {
	int G1;
	bool improved;
	Node* t3;
	Node* t4;
	for(int i=0;i<loopParam[0];i++) {
		t3=nodeById[nMat[t2->id][i]];
		if(t3!=t2->prev && t3!=t2->succ) {
			for(int x4=1;x4<=2;x4++) {
				t4 = x4==1 ? t3->prev : t3->succ;
				if(((t3!=t1 && t4!=t2) || (t3!=t2 && t4!=t1))) {
					G1=dMat[t1->id][t2->id]+dMat[t3->id][t4->id]-dMat[t1->id][t4->id]-dMat[t2->id][t3->id];
					if(x4==1 && G1>0) {
						swap2(t1,t2,t3);
						return true;
					}
					G1+=dMat[t1->id][t4->id];
					improved=do3Opt(t1,t2,t3,t4,x4,G1);
					if(improved)
						return true;
				}
			}
		}
	}
	return false;
}
bool do3Opt(Node* t1, Node* t2, Node* t3, Node* t4, int x4, int G) {
	int G2,c6;
	bool improved;
	Node* t5;
	Node* t6;
	for(int j=0;j<loopParam[1];j++) {
		t5=nodeById[nMat[t4->id][j]];
		if(t5!=t4->prev && t5!=t4->succ && t4!=t2->prev && t4!=t2->succ && !(x4 == 2 && !between(t2,t5,t3))) {
			for(int x6=1;x6<=x4;x6++) {
				if(x4==1) {
					c6=1+!between(t2,t5,t4);
					t6=c6==1?t5->succ:t5->prev;
				}
				else {
					c6=2+x6;
					t6=x6==1?t5->succ:t5->prev;
					if(t6==t1)
						continue;
				}
				G2=G-dMat[t4->id][t5->id]+dMat[t5->id][t6->id]-dMat[t6->id][t1->id];
				if(G2>0) {
					swap3(t1,t2,t3,t4,t5,t6,c6);
					return true;
				}
				G2+=dMat[t6->id][t1->id];
				improved=false;
				if(improved)
					return true;
			}
		}
	}
	return false;
}

void swap2(Node* t1, Node* t2, Node* t3) {
	if (t3 == t2->prev || t3 == t2->succ) {
		return;
	}
	Node *t4 = t1->succ == t2 ? t3->prev : t3->succ;
	if (t1->succ != t2) {
		Node* t=t1;
		t1 = t2;
		t2 = t;
		t = t3;
		t3 = t4;
		t4 = t;
	}
	int R = t2->rank - t3->rank;
	if (R < 0)
		R += N;
	if (R*2 > N) {
		Node* t=t3;
		t3 = t2;
		t2 = t;
		t = t4;
		t4 = t1;
		t1 = t;
	}
	R = t1->rank;
	t1->succ = NULL;
	Node *s1, *s2;
	s2 = t3;
	while ((s1 = s2)) {
		s2 = s1->succ;
		s1->succ = s1->prev;
		s1->prev = s2;
		s1->rank = R--;
	}
	t3->succ = t2;
	t2->prev = t3;
	t1->prev = t4;
	t4->succ = t1;
}
 
void swap3(Node* t1, Node* t2, Node* t3, Node* t4, Node* t5, Node* t6, int c6) {
	switch (c6) {
	case 1:
	case 2:
		swap2(t1, t2, t3);
		swap2(t6, t5, t4);
		return;
	case 3:
		swap2(t1, t2, t4);
		swap2(t6, t5, t4);
		swap2(t6, t2, t3);
		return;
	case 4:
		swap2(t3, t4, t5);
		swap2(t1, t2, t3);
		return;
	}
}